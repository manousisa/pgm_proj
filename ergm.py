import sys
import itertools
import numpy as np
import scipy.special
from scipy.misc import comb
from project import ergm
import dill as pickle
from time import time 
import tensorflow as tf
import matplotlib.pyplot as plt

#import help_me_god as hail_mary
theta_feats = ["n_edges",  "n_mutual_node", "n_two_in_stars", "n_two_out_stars", "n_two_mixed_stars",\
	       "n_transitive_triads", "n_lin_comb_k_trans","n_lin_com_k_trans_alternating",\
               "practice_popularity_effect","seniority_popularity_effect","practice_activity_effect",\
	       "seniority_activity_effect","seniority_similarity_effect","same_office_effect", "same_practice_effect"]


def read_adj_matrix(doc):
    
    arr = []
    f = open(doc,'r')
    for line in f:
        data = line.strip().split(" ")
        data = np.asarray(list(map(float, data)))
        data[data <= 10e-10] = 10e-10
        arr.append(data)
    arr = np.asarray(arr)
    f.close()
    return arr


def read_attr_matrix(doc):

    seniority = []
    practice = []
    office = []

    f = open(doc,'r')
    for line in f:
        data = line.strip().split(" ")
        data = np.asarray(list(map(float, data)))
        data[data <= 10e-10] = 10e-10
        seniority.append(data[4]) 
        practice.append(data[6])
        office.append(data[3])       
    seniority = np.asarray(seniority)
    practice = np.asarray(practice)
    office = np.asarray(office)
    f.close()
    attributes = {'seniority':seniority, 'practice':practice, 'office':office}
    return attributes

def initialize_adj_matrix(shape):

    init = scipy.special.logit(np.random.normal(0.5,0.001, (shape,shape)))
    print(init) 


def diff_Hamiltonian(old_p, old_s, old_theta, new_p, new_s, new_theta):

    old_p = old_p.reshape(71 * 71)
    new_p = new_p.reshape(71 * 71)

    old_K = np.dot(old_p, old_p.T)/2
    new_K = np.dot(new_p, new_p.T)/2

    old_second_term = np.dot(old_theta, old_s)
    new_second_term = np.dot(new_theta, new_s)

 
    return (new_K + new_second_term - old_K - old_second_term)




def rm (theta, x, mu, alpha):
    theta = theta - alpha * (mu - x)
    return {'theta':theta}

def main():
    sess = tf.Session()
    friends = read_adj_matrix('./LazegaLawyers/ELfriend.dat')
    # work =
    work = read_adj_matrix('./LazegaLawyers/ELwork.dat')
    adv = read_adj_matrix('./LazegaLawyers/ELadv.dat')
    attr = read_attr_matrix('./LazegaLawyers/ELattr.dat')
   

    old_X = initialize_adj_matrix(71)
    old_pi = initialize_adj_matrix(71)

    n_iterations = 1 
    step_size = .001
    ergm_obj = ergm(work, attr)
    feats = ergm_obj.extract_features(work,sess)
    print(len(feats))
    grad_tensor = np.empty((1, 71, 71), float)
    s_feats = []

   
    for grads in range(1,len(feats),2):
        if grads == 1:
            grad_tensor = np.asarray(feats[grads])
        else:
            grad_tensor = np.vstack((grad_tensor, np.asarray(feats[grads])))

    for plain_feats in range(0, len(feats), 2):
        s_feats.append(feats[plain_feats])
    s_feats = np.asarray(s_feats)

    #Sufficient stats for graph add your favorite stats.
    ss_obs = s_feats

    # JUST TO ADD NOISE TO THE GRAPH
    # s_feats = s_feats + np.random.randn(s_feats.shape[0])
    
    #coefficients to fit
    coefs = np.asarray([0.] * len(ss_obs))     
    dedx = np.einsum('ij, jkl->kl', coefs.reshape(1, -1), grad_tensor)
    old_X = np.random.beta(.5, .5, size=friends.shape)
    thetas = [] 

    #Fitting process
    t = time()


    number_of_outer_loops = 30
    num_of_inner_loops = 1000

    for outer_iter in range(number_of_outer_loops): #Replace with number of outer iterations
       
        inner_log = []
        X_arr = []
        p_arr = []
        if outer_iter > 0:
            current_feat = new_s_feats
        else:
            current_feat = s_feats

        acceptance_ctr = 1
        while(acceptance_ctr <= num_of_inner_loops):
        #for inner_iter in range(50000): # n_iterations):
            if (acceptance_ctr % 10 == 0):
                print(time() - t)
                t = time() 
                print("acceptance ", acceptance_ctr)
            #Sample from \pi(p|x)
            old_p = np.random.randn(71*71)
            old_p = old_p.reshape(71,71)
     
            #Update params 


            new_X = old_X + step_size * old_p
            new_p = old_p - step_size * (old_p + dedx)

            new_s = ergm_obj.extract_features(new_X, sess)  # get_features(new_X)
            new_s_feats = []
            

            for plain_feats in range(0, len(new_s), 2):
                new_s_feats.append(new_s[plain_feats])
            new_s_feats = np.asarray(new_s_feats)

            for grads in range(1, len(new_s), 2):
                if grads == 1:
                    new_grad_tensor = np.asarray(new_s[grads])
                else:
                    new_grad_tensor = np.vstack((new_grad_tensor, np.asarray(new_s[grads])))

            tmp_dedx = np.einsum('ij, jkl->kl', coefs.reshape(1, -1), new_grad_tensor)

            #new_p = new_p -0.5*step_size*(new_p + tmp_dedx)

            #Accept/reject
            tmp = diff_Hamiltonian(old_p, current_feat, coefs, new_p, new_s_feats, coefs)

            decision = min(1, np.exp(tmp))
            u = np.random.uniform(0,1)

            if u < decision:
                # print ("helloooo")
                X_arr.append(new_X)
                p_arr.append(new_p)
                dedx = tmp_dedx
                acceptance_ctr += 1
                old_X = new_X
                old_p = new_p
                if (acceptance_ctr == num_of_inner_loops + 1) or (outer_iter == number_of_outer_loops - 1):
                    inner_log.append(params)
            params = {'new_X': new_X, 'new_p': new_p}
        # print (np.histogram(new_X.sum(0)))
        # print (np.histogram(new_X.sum(1)))
      
        # X_arr = X_arr[1000:]
        # p_arr = p_arr[1000:]
	## Add line of code about rm algorithm
        coefs = rm(np.asarray(coefs), np.asarray(s_feats), np.asarray(new_s_feats), 1. / (outer_iter + 1.))['theta']
        if outer_iter == 4:
           print("bla")
           print(coefs)
           print(theta_feats)
            #plt.figure()
            ##plt.title(str(outer_iter) + " " + "0")
            #plt.hist(new_X.sum(0), bins=20)
            #plt.figure()
            #plt.title(str(outer_iter) + " " + "1")
            #plt.hist(new_X.sum(1) ,bins=20)
        thetas.append([coefs,inner_log])
    #plt.show()

#    print("LOG")
    pickle.dump(thetas, open( "log.p", "wb" ) )


if __name__ == "__main__":
    main() 
