from __future__ import print_function
import pickle
import scipy.io as sio
import tensorflow as tf
from project import ergm
import numpy as np
from ergm import read_adj_matrix, read_attr_matrix
from matplotlib import pyplot as plt
full_list = pickle.load(open("log.p", "rb"))

# extracting the final step
coefs,inner_log = full_list[-1]
# print (inner_log[-50:])
# print (coefs)
sio.savemat("X_matrix.mat", {'X': [element['new_X'] for element in inner_log[::20]]})
# print ([element['new_X'] for element in inner_log[-50:]], file=open('text_results.txt', "w"))
# pickle.dump([coefs, [element['new_X'] for element in inner_log[-50:]]], open("final.p", "wb"), protocol=2)
plt.hist(inner_log[-1]['new_X'].sum(0), bins=20)
plt.show()
#


# coefs, X_samples = pickle.load(open("final.p", "rb"))
# print (X_samples)