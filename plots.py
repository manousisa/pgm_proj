"""The code for plotting the features compared to their relevant param!"""
import tensorflow as tf
from project import ergm
import numpy as np
import dill as pickle
from ergm import read_adj_matrix, read_attr_matrix, initialize_adj_matrix
from matplotlib import pyplot as plt
full_list = pickle.load(open("log.p", "rb"))

sess = tf.Session()

friends = read_adj_matrix('./LazegaLawyers/ELfriend.dat')
work = read_adj_matrix('./LazegaLawyers/ELwork.dat')
adv = read_adj_matrix('./LazegaLawyers/ELadv.dat')
attr = read_attr_matrix('./LazegaLawyers/ELattr.dat')

old_X = initialize_adj_matrix(71)
old_pi = initialize_adj_matrix(71)

title_list =   ['# of Mutual Dyads',
                  '# of 2-in stars',
                  '# of 2-outstars',
                  '# of 2-mixed stars',
                  "# of k-alternating paths",
                  "# of k-alternating transitive paths",
                 '# of geometric weighted out',
                 '# of geometric weighted in',
                  'practice popularity',
                  'seniority popularity effect',
                  'practice activity effect',
                  'seniority_activity_effect',
                  'seniority_similarity_effect',
                  'same_office_effect',
                  'same_practice_effect']


ergm_obj = ergm(work, attr)
theta_coeff_list = []
for coefs, inner_log in full_list:
    temp_list = []
    X = inner_log[-1]['new_X']
    tf_output = list(ergm_obj.extract_features(X, sess))
    for i in np.arange(0, len(tf_output), 2):
        temp_list.append(tf_output[i])
    theta_coeff_list.append([coefs, temp_list])
fig, ax = plt.subplots(3, 5)
for i in range(3):
    for j in range(5):
        for coefs, feature_list in theta_coeff_list:
            ax[i, j].scatter(coefs[i*5+j], feature_list[i*5+j], color='b')
            ax[i, j].set_xlabel(title_list[i*5+j])
# plt.tight_layout()
plt.show()



