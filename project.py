import tensorflow as tf
import numpy as np
import os
os.environ["CUDA_VISIBLE_DEVICES"]="-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf


class ergm():
    def __init__(self, A, atts):
        """
        :param A: the adjacency matrix
        :param atts: A dictionary of attribute matrices
        """
        self.A_tensor = tf.placeholder(tf.float32, shape=A.shape)

        self.seniority_similarity = tf.constant(self.similarity_continuous(atts['seniority']), dtype=tf.float32)
        self.same_office = tf.constant(self.similarity_discrete(atts['office']), dtype=tf.float32)
        self.same_practice = tf.constant(self.similarity_discrete(atts['practice']), dtype=tf.float32)
        

        # converting dict of attribute vectors to dict of attrbute tensors        
        self.atts_TF = dict()
        for key in atts.keys():
            self.atts_TF[key] = tf.constant(atts[key], dtype=tf.float32)
        # self.extract_features()
        self.define_features()

    # Fixed edge covariates:
    def similarity_continuous(self, attribute):
        n_nodes = attribute.shape[0]
        cont_similarity = np.zeros([n_nodes, n_nodes])
        for i in np.arange(n_nodes):
            for j in np.arange(n_nodes):
                cont_similarity[i, j] = np.abs(attribute[i] - attribute[j])
        cont_similarity = 1 - cont_similarity / np.max(cont_similarity)
        return cont_similarity

    def similarity_discrete(self, attribute):
        n_nodes = attribute.shape[0]
        disc_similarity = np.zeros([n_nodes, n_nodes])
        for i in np.arange(n_nodes):
            for j in np.arange(n_nodes):
                disc_similarity[i, j] = 1 * (attribute[i] == attribute[j])
        return disc_similarity


    def calculate_grad(self, feature):
        return tf.gradients(feature, self.A_tensor, stop_gradients=self.A_tensor)

    def define_features(self):
        """
        Compute the number of edges in G.

        Args:
          input_A: A 2d numpy array representing an adjacency matrix that we extract its features

        Returns:
          A feature values and their gradient

        """

        # calculation of u(G) according to (https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwicidjBxejaAhWrc98KHYG-CD8QFggxMAA&url=http%3A%2F%2Fwww.stats.ox.ac.uk%2F~snijders%2Fsiena%2Fp12_ergm_ha.pdf&usg=AOvVaw19Utsi1iKzsNyPEEYK-ac4 page 85/108)
        lambda_ = 2.
        alpha = np.log(2)

        # Actor attribute terms
        def activity_effect(attribute):
            return tf.reduce_sum(tf.multiply(attribute, tf.reduce_sum(self.A_tensor, axis=0)))

        def popularity_effect(attribute):
            return tf.reduce_sum(tf.multiply(attribute, tf.reduce_sum(self.A_tensor, axis=1)))

        def similarity_effect(similarity):
            return tf.reduce_sum(tf.multiply(similarity, self.A_tensor))


        # final tensors for extracting attribute features and their gradient



        self.practice_popularity_effect = popularity_effect(self.atts_TF['practice'])
        self.practice_popularity_effect_grad = self.calculate_grad(self.practice_popularity_effect)

        self.seniority_popularity_effect = popularity_effect(self.atts_TF['seniority'])
        self.seniority_popularity_effect_grad = self.calculate_grad(self.seniority_popularity_effect)

        self.practice_activity_effect = activity_effect(self.atts_TF['practice'])
        self.practice_activity_effect_grad = self.calculate_grad(self.practice_activity_effect)

        self.seniority_activity_effect = activity_effect(self.atts_TF['seniority'])
        self.seniority_activity_effect_grad = self.calculate_grad(self.seniority_activity_effect)

        self.seniority_similarity_effect = similarity_effect(self.seniority_similarity)
        self.seniority_similarity_effect_grad = self.calculate_grad(self.seniority_similarity_effect)

        self.same_office_effect = similarity_effect(self.same_office)
        self.same_office_effect_grad = self.calculate_grad(self.same_office_effect)

        self.same_practice_effect = similarity_effect(self.same_practice)
        self.same_practice_effect_grad = self.calculate_grad(self.same_practice_effect)

        self.n_edges_node = tf.reduce_sum(self.A_tensor)
        self.n_edges_grad = self.calculate_grad(self.n_edges_node)


        # ************************************************
        # """Compute the number of mutual edges in G.
        #
        # Args:
        #   G: A 2d numpy array representing an adjacency matrix.
        # Returns:
        #   A float."""
        # ************************************************

        # OLD CODE

        # sum = 0.
        # for i in xrange(G.shape[0]):
        #     for j in xrange(G.shape[1]):
        #         sum += G[i, j] * G[j, i]
        AA = tf.matmul(self.A_tensor, self.A_tensor)
        AA_T = tf.matmul(self.A_tensor, tf.transpose(self.A_tensor))
        A_TA = tf.matmul(tf.transpose(self.A_tensor), self.A_tensor)

        self.n_mutual_node = tf.trace(AA_T)
        self.n_mutual_grad = self.calculate_grad(self.n_mutual_node)


        # """
        # Compute the number of two-in-stars in G.
        # Args:
        #   G: A 2d numpy array representing an adjacency matrix.
        # Returns:
        #   A float.
        # """
        # sum = 0.
        # incomings = G.sum(0)  # sum over rows
        # for incoming in incomings:
        #     sum += comb(incoming, 2)

        outgoings = tf.reduce_sum(self.A_tensor, 0)
        incomings= tf.reduce_sum(self.A_tensor, 1)

        self.geom_weighted_out = tf.reduce_sum(tf.exp(-alpha * outgoings))
        self.geom_weighted_out_grad = self.calculate_grad(self.geom_weighted_out)

        self.geom_weighted_in = tf.reduce_sum(tf.exp(-alpha * incomings))
        self.geom_weighted_in_grad = self.calculate_grad(self.geom_weighted_in)


        self.n_two_in_stars = 0.5 * (tf.reduce_sum(AA_T) - tf.trace(AA_T))
        self.n_two_in_stars_grad = self.calculate_grad(self.n_two_in_stars)

        self.n_two_out_stars = 0.5 * (tf.reduce_sum(A_TA) - tf.trace(AA_T))
        self.n_two_out_stars_grad = self.calculate_grad(self.n_two_out_stars)

        self.n_two_mixed_stars = 0.5 * (tf.reduce_sum(AA) - tf.trace(AA))
        self.n_two_mixed_stars_grad = self.calculate_grad(self.n_two_mixed_stars)


        # """
        # Compute the number of cyclic triads in G.
        # Args:
        #   G: A 2d numpy array representing an adjacency matrix.
        # Returns:
        #   A float.
        # """

        A_tensor_cubed = tf.matmul(self.A_tensor, AA)

        # self.n_cycalic_triads = tf.reduce_sum(A_tensor_cubed) / 3.
        # self.n_cycalic_triads_grad = self.calculate_grad(self.n_cycalic_triads)


        # calculating the number tranisitve triads (for details see page 4 of https://eml.berkeley.edu/~bgraham/Teaching/CEMFI_May2014/CEMFI_Social_Networks_Lecture_1.pdf)



        # G_squared = tf.matmul(self.A_tensor, self.A_tensor)
        lin_comb_k_trans_mat = tf.multiply(self.A_tensor, 1. - tf.pow((1. - 1. / lambda_), AA-tf.diag_part(AA)))

        self.n_lin_comb_k_trans = lambda_ * tf.reduce_sum(lin_comb_k_trans_mat)
        self.n_lin_comb_k_trans_grad = self.calculate_grad(self.n_lin_comb_k_trans)

        # implamentation of The corresponding statistic
        # with alternating and geometrically decreasing weights is from page 84/108
        lin_com_k_trans_alternating_mat =  (1. - tf.pow((1. - 1. / lambda_), AA-tf.diag_part(AA)))

        self.n_lin_com_k_trans_alternating = lambda_ * tf.reduce_sum(lin_com_k_trans_alternating_mat)
        self.n_lin_com_k_trans_alternating_grad = self.calculate_grad(self.n_lin_com_k_trans_alternating)


    def extract_features(self, input_A, sess):
        return sess.run([self.n_edges_node, self.n_edges_grad,
                  self.n_mutual_node, self.n_mutual_grad,
                  self.n_two_in_stars, self.n_two_in_stars_grad,
                  self.n_two_out_stars, self.n_two_out_stars_grad,
                  self.n_two_mixed_stars, self.n_two_mixed_stars_grad,
                  self.n_lin_comb_k_trans, self.n_lin_comb_k_trans_grad,
                  self.n_lin_com_k_trans_alternating, self.n_lin_com_k_trans_alternating_grad,
                  self.geom_weighted_out, self.geom_weighted_out_grad,
                  self.geom_weighted_in, self.geom_weighted_in_grad,

                  self.practice_popularity_effect, self.practice_popularity_effect_grad,
                  self.seniority_popularity_effect, self.seniority_popularity_effect_grad,
                  self.practice_activity_effect, self.practice_activity_effect_grad,
                  self.seniority_activity_effect, self.seniority_activity_effect_grad,
                  self.seniority_similarity_effect, self.seniority_similarity_effect_grad,
                  self.same_office_effect, self.same_office_effect_grad,
                  self.same_practice_effect, self.same_practice_effect_grad
                    ], feed_dict={self.A_tensor: input_A})


if __name__ == "__main__":
    A = np.asarray([[1., .2],[3.,.3]])
    att = dict()
    att['seniority'] = np.asarray([1., 2.])
    att['office'] = np.asarray([1., 0.])
    att['practice'] = np.asarray([1., 2.])
    test_ergm = ergm(A, atts=att)
    sess = tf.Session()
    print (test_ergm.extract_features(A+.1, sess))
    print (test_ergm.extract_features(A+.5, sess))
    print (test_ergm.extract_features(A+0.1, sess))


